import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class PersonaFiltroListado extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.    
    }    

    render() {

        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        return html`  
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">                    
        <div class="col-5">
            <fieldset>
                <legend>Filtros</legend>                
                <label for="filtroPorAño" >Por años en la empresa:</label>
                <label id="filtroporAñoViewValue"></label>                            
                <input id="filtroPorAño" @input="${this.filtrarListado}" type="range" class="form-range" min="1" max="15" step="1" value="15" >                    
            </fieldset>                
            
        </div>        
        <div class="bg-light clearfix col-auto d-flex flex-row-reverse" aku >
            <button @click="${this.limpiarFiltros}" type="button" class="btn btn-primary" ><strong>Limpiar filtros</strong></button>
        </div>        
        <br />
        `;
    } 

    filtrarListado(e){
        console.log("filtrarListado");
        console.log(e.target.value);

        let filtro = e.target.value;
        this.shadowRoot.getElementById("filtroporAñoViewValue").innerText = e.target.value + " años";

        this.dispatchEvent(new CustomEvent("persona-list-filter",{
            detail: {
                filters:{
                    byYearInCompany: filtro                    
                }
            }
        }));
    }

    limpiarFiltros(e){
        console.log("limpiarFiltros");
        e.preventDefault();

        this.shadowRoot.getElementById("filtroporAñoViewValue").innerText = "";

        this.shadowRoot.getElementById("filtroPorAño").value = "0";
        
        this.dispatchEvent(new CustomEvent("persona-list-clean-filter",{}));        
    }
}

customElements.define('persona-filtro-listado', PersonaFiltroListado);