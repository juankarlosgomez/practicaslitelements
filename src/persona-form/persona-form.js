import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.            
        this.inicializarForm();
    }    

    render() {
        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        //? en los atributos booleanos   
        //. para asignar el valor a un atributo/propiedad     
        return html`  
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input type="text" class="form-control" placeholder="Nombre completo"
                            @input="${this.updateName}" 
                            .value="${this.person.name}"
                            ?disabled="${this.editingPerson}"/>
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea class="form-control" placeholder="Perfil" rows="5"
                            @input="${this.updateProfile}"
                            .value="${this.person.profile}"></textarea>                        
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="number" class="form-control" placeholder="Años en la empresa" 
                            @input="${this.updateYearsInCompany}"
                            .value="${this.person.yearsInCompany}"
                            />
                    </div>
                    <br />
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }        

    updateName(e){
        console.log("udpateName");
        console.log("actualizando la propiedad name con el valor " + e.target.value);

        this.person.name = e.target.value;
    }

    updateProfile(e){
        console.log("updateProfile");
        console.log("actualizando la propiedad profile con el valor " + e.target.value);

        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYears");
        console.log("actualizando la propiedad yearsInCompany con el valor " + e.target.value);

        this.person.yearsInCompany = e.target.value;
    }

    goBack(e){
        console.log("goBack en persorna.form");
        e.preventDefault(); //para evitar el submit

        this.dispatchEvent(new CustomEvent("persona-form-close",{}));

        this.inicializarForm();

    }

    storePerson(e){
        console.log("storePerson en persorna.form");
        e.preventDefault(); //para evitar el submit

        console.log(this.person);

        this.person.photo = {
            "src": "./img/persona.png",
            "alt": "Homer"
        }

        this.dispatchEvent(new CustomEvent("persona-form-store",{
                detail: {
                    person:{
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo,
                        personId: Math.round(Math.random() * (10000 - 100) + 100)//this.person.personId
                    },
                    editingPerson: this.editingPerson
                }
            }
        ));

        this.inicializarForm();

    }

    inicializarForm(){
        console.log("inicializarForm");

        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";
        this.person.personId = "";

        this.editingPerson = false;
    }
}

customElements.define('persona-form', PersonaForm);