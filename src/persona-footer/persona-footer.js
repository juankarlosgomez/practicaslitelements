import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.    
    }    

    render() {

        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        return html`
            <h5>@PersonaApp 2021</h5>
        `;
    }        
}

customElements.define('persona-footer', PersonaFooter);