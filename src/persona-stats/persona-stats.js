import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people: {tyoe: Array}
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.    

        this.people = [];
    }

    updated(changedProperties){
        console.log("updated en persona-stats");
        if (changedProperties.has("people")){
            console.log("Ha cambiado la propiedad people en persona-stats");
            
            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people-stats",{
                detail:{
                    peopleStats: peopleStats
                }
            }));
        }
    }

    gatherPeopleArrayInfo(people){
        console.log("gatherPeopleArrayInfo");

        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;

    }
     
}

customElements.define('persona-stats', PersonaStats);