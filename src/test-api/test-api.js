import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class TestApi extends LitElement {

    static get properties() {
        return{
            movies: {type: Array}
        };
    }

    constructor(){
        super();

        //Inicializamos el array
        this.movies = [];
        this.getMovieData();
    }

    render() {
        return html `
            <h2>Películas StarWars</h2>
            <br />
            ${this.movies.map(
                movie => html`<div>La película ${movie.title} fue dirigida por ${movie.director}</div>`
            )}       
        `;
    }    
    
    getMovieData(){
        console.log("getMovieData");
        console.log("Obteniendo datos de las peliciulas");

        //let -> el ambito es el bloque
        //var -> en la función
        //si no se pone nada.. sería global
        let xhr =  new XMLHttpRequest();
        xhr.onload = () => {
            if (xhr.status === 200){
                console.log("Petición completada correctamente");
                console.log(xhr);

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                
                this.movies = APIResponse.results;

            }else{
                console.log("No se han podido lanzar la petición");
            }
        }
        xhr.open("GET","https://swapi.dev/api/films/");
        xhr.send();
        
        console.log("fin de getMovieData");
    }

}

customElements.define('test-api', TestApi);