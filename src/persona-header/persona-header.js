import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class PersonaHeader extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.    
    }    

    render() {

        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        return html`  
        `;
    }        
}

customElements.define('persona-header', PersonaHeader);