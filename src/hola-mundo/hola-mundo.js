import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class HolaMundo extends LitElement {

    render() {
        return html `<div>Hola Mundo!</div>`;
    }    
}

customElements.define('hola-mundo', HolaMundo);