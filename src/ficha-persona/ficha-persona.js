import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String}
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.

        //Se inicializan por aquí, valor por defecto, pero se pueden poner como propiedad en el html de la
        //pagina donde se usa el componente, al renderizar se sobreescribirá el valor.
        this.name = "prueba Nombre";
        this.yearsInCompany = "12";        
    }

    //Evento para cazar los cambios en las propiedades
    //quizás buen sitio para recalcular propiedades en base a otras, como personInfo
    updated(changedProperties) {
        console.log("updated");

        //bucle para ver todas las propiedades modificadas
        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior " + oldValue);
        });

        //se puede comprobar si una en cocreta ha cambiado
        if (changedProperties.has("name")){
            console.log("Propiedad Name ha cambiado, valor anterior " + changedProperties.get("name")
            + " nuevo es " + this.name);
        }

        if (changedProperties.has("yearsInCompany")){
            this.updatePersonInfo();
        }
        
    }

    render() {

        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        return html`
            <div>
                <label>Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"/>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"/>
                <br />
                <input type="text" value="${this.personInfo}" disabled />
                <br />
            </div>
        `;
    }    

    //e sería el input en este caso
    updateName(e){
        console.log("updateName");
        this.name = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo(){
        if (this.yearsInCompany >=7){
            this.personInfo = "lead";
        } else if (this.yearsInCompany >=5){
            this.personInfo = "senior";
        } else if (this.yearsInCompany >=3){
            this.personInfo = "team";
        } else{
            this.personInfo = "junior";
        }
    }
}

customElements.define('ficha-persona', FichaPersona);