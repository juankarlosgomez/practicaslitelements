import { LitElement, html} from 'lit-element'; //por defecto mira en node-modules
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-filtro-listado/persona-filtro-listado.js';

class PersonaMain extends LitElement {
    
    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
            filters: {type: Object}
        };
    }

    constructor(){
        super(); //Llamamos al constuctor de la clase base, en este caso de LitElements.    

        this.cargarPeople();
        this.filters = {};
       
        this.showPersonForm = false;
    }    

    render() {

        //@change se ejecuta al perder el foco del campo
        //@input se ejecuta al teclear
        //map se puede usar para recorrer un array
        //la propiedad photo en vez de pasarlo como atributo, lo pasamos como propiedad indicando un . por 
        //delante, de esa forma el parseador de LitE es capaz de parsearlo como objeto
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row">
                <persona-filtro-listado id="personFilters" @persona-list-filter="${this.personaListFilter}"
                                        @persona-list-clean-filter="${this.personaCleanFilter}"></persona-filtro-listado>
            </div>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4" id="">
                ${this.people.filter(person => (this.filters && this.filters.byYearInCompany && person.yearsInCompany <= this.filters.byYearInCompany) || this.filters.byYearInCompany === undefined)
                    .map(
                            person => 
                                html `<persona-ficha-listado 
                                    fname="${person.name}"
                                    yearsInCompany="${person.yearsInCompany}"  :
                                    profile = "${person.profile}"
                                    .photo="${person.photo}"
                                    personId="${person.personId}"
                                    @delete-person="${this.deletePerson}"
                                    @info-person="${this.infoPerson}"></persona-ficha-listado>`
                                    
                        )}                
                </div>
            </div>    
            <div class="row">
                <persona-form id="personForm" class="d-none border rounded border-primary"
                                 @persona-form-close="${this.personFormClose}"
                                 @persona-form-store="${this.personFormStore}">
                </persona-form>
            </div>
        `;
    }

    cargarPeople(){
        this.people = [
            {
                personId: 0,
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Ellen Ripley"
                }
                
            },{
                personId: 1,
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile: "Lorem ipsum dolor sit amet consectetur, adipiscing elit curae aptent.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Bruce Banner"
                }
            },{
                personId: 2,
                name: "Éowyn",
                yearsInCompany: 5,
                profile: "adipiscing elit curae aptent.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Éowyn"
                }
            },{
                personId: 3,
                name: "Turanga Leola",
                yearsInCompany: 9,
                profile: "metus diam nisi accumsan.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Turanga Leola"
                }
            },{
                personId: 4,
                name: "Tyrion Lannister",
                yearsInCompany: 15,
                profile: "Lorem ipsum dolor sit amet consectetur adipiscing elit, velit class ornare ante hendrerit malesuada, sollicitudin suscipit praesent diam iaculis vestibulum. Faucibus rhoncus donec montes etiam, vitae senectus.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Tyrion Lanniste"
                }
            }
        ];
    }

    //Evento litElemnts
    updated(changedProperties){
        console.log("updated persona-main");

        if (changedProperties.has("showPersonForm")){
            console.log("Han cambiado el valor de la propiedad showPersonForm en persona-main");
            if(this.showPersonForm === true){
                this.showPersonFormData();
            }else{
                this.showPersonList();
            }
        }
        //Para que LitElements detecte un cambio en un array, se debe asignar el array de nuevo        
        if (changedProperties.has("people")){
            console.log("Han cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people",{
                detail: {
                    people: this.people
                }})
            );
        }
    }

    personaCleanFilter(e){
        console.log("personaCleanFilter en persona-main");
        this.filters = {};
    }

    personaListFilter(e){
        console.log("personaListFilter en persona-main");
        console.log(e.detail.filters);

        //this.cargarPeople();

        this.filters = e.detail.filters;

        //if (e.detail.filters.byYearInCompany !== undefined) {
        //    console.log("Filtrando por yearsInCompany");
        //    this.people = this.people.filter(
        //        person => person.yearsInCompany  == e.detail.filters.byYearInCompany
        //    );
        //}

    }

    personFormStore(e){
        console.log("personFormStore en persona-main");
        console.log(e.detail.person);

        if (e.detail.editingPerson === true){
            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person)
            
            //let indexOfPerson = this.people.findIndex(
            //    person => person.name === e.detail.person.name
            //);

            //if (indexOfPerson >= 0 ){
            //    this.people[indexOfPerson] = e.detail.person;
            //    console.log("Persona actualizada");
            //}            
        }else{
            console.log("Perdona insertada");
            //this.people.push(e.detail.person);
            //... descompone el array en elementos para añadir luego person
            this.people = [...this.people, e.detail.person];
        }        

        this.showPersonForm = false;        
    }

    personFormClose(){
        console.log("personFormClose en persona-main");
        this.showPersonForm = false;
    }

    showPersonList(){
        console.log("showPersonList");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personFilters").classList.remove("d-none");
        
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personFilters").classList.add("d-none");
    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name + " " + e.detail.personId);

        //si hiciera un splice del porpia this.people
        //luego debería hacer un this.requestUpdate(); para que se actualice en pantalla
        
        this.people = this.people.filter(
            person => person.personId != e.detail.personId
        );
    }

    infoPerson(e){
        console.log("Info-person en persona-main");
        console.log(e.detail);

        //aqui deberiamos filtrar por id
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;
        person.personId = chosenPerson[0].personId;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;

    }
}

customElements.define('persona-main', PersonaMain);