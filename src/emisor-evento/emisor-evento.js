import { LitElement, html } from 'lit-element' //por defecto mira en node-modules

class EmisorEvento extends LitElement {

    static get properties() {
        return{
        };
    }

    constructor(){
        super();
    }

    render() {
        return html `
            <h3>Emisor Evento!</h3>
            <button @click="${this.sendEvent}">No pulsar</button>
        `;
    }    

    sendEvent(e){
        console.log("sendEvent");
        console.log(e);

        this.dispatchEvent(
            new CustomEvent(
                "test-event", //nombre del custom event y el nombre del evento que capturaremos desde otro componente
                {
                    "detail": { //sería el payload del evento
                        "course": "TechU",
                        "year": 2021
                    }
                }
            )
        );
    }
}

customElements.define('emisor-evento', EmisorEvento);